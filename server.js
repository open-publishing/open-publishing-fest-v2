// const cron = require("node-cron");
const express = require("express");
const axios = require("axios");
const Path = require("path");
const fs = require("fs");
const ics = require("ics");
const moment = require("moment-timezone");
const configuration = require("./src/data/config.json");

const runScript = require("runscript");

const folder = "static/data/events/images/";
const folderAttendee = "static/data/attendees/images/";
const folderICS = "static/data/events/ics/";

// get password and login from .env

require("dotenv").config();

// start express
app = express();
app.listen(configuration.strapi.port);

if (!fs.existsSync(`./${folder}`)) {
  fs.mkdirSync(folder, { recursive: true });
}
if (!fs.existsSync(`./${folderAttendee}`)) {
  fs.mkdirSync(folderAttendee, { recursive: true });
}
if (!fs.existsSync(`./${folderICS}`)) {
  fs.mkdirSync(folderICS, { recursive: true });
}

let html = `
<button type="button" onclick="proceed();">do</button> 
<script>
function proceed () {
    var form = document.createElement('form');
    form.setAttribute('method', 'post');
    form.setAttribute('action', '/');
    form.style.display = 'hidden';
    document.body.appendChild(form)
    form.submit();
}
</script>


<button type="button" onclick="pull();">pull repo</button> 
<script>
function pull () {
    var pullForm = document.createElement('form');
    pullForm.setAttribute('method', 'post');
    pullForm.setAttribute('action', '/pull/');
    pullForm.style.display = 'hidden';
    document.body.appendChild(pullForm)
    pullForm.submit();
}
</script>`;

// login now use a token

// get the token from strapi
let token = process.env.STRAPITOKEN;

// login on start
login(token);

async function login(token) {
  load(token);
  loadAttendees(token);
}

async function load(token) {
  axios
    .get(
      `${configuration.strapi.server}/api/${configuration.strapi.apiEvents}?populate=deep`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    )
    .then(function(response) {
      const data = response.data.data;
      let flatdata = data.map((item) => {
        const imageUrl = item.attributes.images.data.attributes.url;
        const imageName =
          item.attributes.images.data.attributes.hash +
          item.attributes.images.data.attributes.ext;
        const calendarid = item.attributes.calendar.data.id;
        const { attributes, ...rest } = item;
        return {
          imageUrl,
          imageName,
          calendarid,
          ...rest,
          ...attributes,
        };
      });
     // to test why this is not workin on the server
      backupDB(`src/data/events.json`, flatdata);
      backupIMG(flatdata);
      createICS(flatdata);
    })
    .finally(
      runScript("npm run buildPerm", { stdio: "pipe" })
        .then((stdio) => {
          console.log("website is done, now updating permission");
        })
        .catch((err) => {
          console.error(err);
        })
    )
    .catch(function(error) {
      console.log("error:", error);
    });
}
async function loadAttendees(token) {
  axios
    .get(
      `${configuration.strapi.server}/api/${configuration.strapi.apiParticipants}?populate=deep`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    )
    .then(function(response) {
      const data = response.data.data;
      let flatdata = data.map((item) => {
        const imageUrl = item.attributes.picture.data.attributes.url;
        const imageName =
          item.attributes.picture.data.attributes.hash +
          item.attributes.picture.data.attributes.ext;
        const { attributes, ...rest } = item;
        return {
          imageUrl,
          imageName,
          ...rest,
          ...attributes,
        };
      });

      backupDBAttendees(`src/data/attendees.json`, flatdata);
      backupIMGAttendees(flatdata);
      // console.log("response", response);
    })
    .catch(function(error) {
      console.log("error:", error);
    });
}

async function backupDB(filename, data) {
  data.forEach((event) => {
    // remove refused events

    if (event.accept != true) {
      // data.splice(data[index], 1)
    }
    // obfuscate user contact for the events (not accessible in the js anymore!)
    event.usermail = " ";

    // create a fullTime key to handle calendar creation and update serverside
    event.fullTime = moment
      .tz(`${event.date}T${event.time}`, event.timezone)
      .utc()
      .format();
  });

  // reorder events

  let content = JSON.stringify(orderByDate(data));

  fs.writeFileSync(filename, content),
    "utf-8",
    function(err) {
      if (err) {
        return console.log(err);
      }
      console.log("script is done!");
    };
}

function orderByDate(dates) {
  return dates.sort((a, b) => new moment(a.fullTime).diff(b.fullTime));
}

async function backupIMG(data) {
  data.forEach((img) => {
    downloadImage(
      configuration.strapi.server + img.imageUrl,
      img.imageName,
      folder
    );
  });
}

async function backupDBAttendees(filename, data) {
  data.forEach((attendee) => {
    attendee.mail = " ";
  });
  let content = JSON.stringify(data);
  fs.writeFileSync(filename, content),
    "utf-8",
    function(err) {
      if (err) {
        return console.log(err);
      }
      console.log("The file was saved!");
    };
}
async function backupIMGAttendees(data) {
  data.forEach((img) => {
    downloadImage(
      configuration.strapi.server + img.imageUrl,
      img.imageName,
      folderAttendee
    );
  });
}

async function createICS(data) {
  data.forEach((ev) => {
    // console.log('event', ev);
    const dateStart = moment.tz(`${ev.date}T${ev.time}`, ev.timezone).utc();

    // convert dates to array
    startTime = [
      dateStart.get("year"),
      dateStart.get("month") + 1,
      dateStart.get("date"),
      dateStart.get("hour"),
      dateStart.get("minute"),
    ];
    // console.log(startTime);

    ics.createEvent(
      {
        title: ev.title,
        description:
          ev.description +
          `  \n\nWARNING: TIME AND DATES MAY CHANGE, \nBE SURE TO CHECK THE EVENT ON THE WEBSITE: \n\n${configuration.siteURL}/calendar.html#event-${ev.id}\n\n and get the latest news on ${configuration.siteURL}/calendar.html#event-${ev.id}`,
        busyStatus: "FREE",
        start: startTime,
        URL: ev.link,
        startInputType: "utc", // I define the start time as UTC
        duration: { minutes: 60 },
        // start: [2000, 05,16,10, 30],
      },
      (error, value) => {
        if (error) {
          console.log(error);
        }

        fs.writeFileSync(`${__dirname}/${folderICS}/event-${ev.id}.ics`, value);
        console.log("ics created:", ev.title);
      }
    );
  });
}

app.use(express.static("events"));

app.get("/", function(req, res) {
  res.send(html);
});

app.get("/rebuild/", function(res, req) {
  login(token);
});

app.get("/pullDaRepo/", function(res, req) {
  runScript("npm run pullRepo", { stdio: "pipe" })
    .then((stdio) => {
      // console.log("pull the repo");
      // console.log(stdio);
    })
    .then(login(token))
    .catch((err) => {
      console.error(err);
    });
});

app.post("/", function(req, res) {
  login(token);
  res.send("done");
});

app.get("/pull/", function(req, res) {
  res.send(html);
});

app;

async function downloadImage(url, file, location) {
  console.log("url, file, location");
  console.log(url, file, location);
  const path = Path.resolve(__dirname, location, file);
  const writer = fs.createWriteStream(path);

  console.log("image copied:", url);

  const response = await axios({
    url,
    method: "GET",
    responseType: "stream",
  });

  response.data.pipe(writer);

  return new Promise((resolve, reject) => {
    writer.on("finish", resolve);
    writer.on("error", reject);
  });
}
