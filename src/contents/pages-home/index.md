---
layout: index.njk
class: "home splash"
permalink: /index.html
title: Open Publishing Fest
subtitle: Open Knowledge  ≈ / = Open Publishing ?
dates: 2023
tags:
  - intro
---

Open Publishing Fest is a decentralized public event that brings together communities supporting open source software, open content, and open publishing models.

Held during two weeks in June this year, Open Publishing Fest will feature discussions, demos, and performances that showcase our paths toward a more open world.

<p class="bigger">Open Knowledge ≈/= Open Publishing ?</p>

<!-- For more info or to discuss ideas please email <a href="mailto:adam@coko.foundation">adam@coko.foundation</a>. -->


<!-- <p class="proposal-wrapper"><a class="proposal" href="form.html">Propose an event!</a> -->


