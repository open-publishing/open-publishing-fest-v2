// markdown-system

//plugins
const markdownHandling = require("./plugins/markdown.js");
const handlingTime = require("./plugins/handlingTime.js");
const helpers = require("./plugins/helpers.js");
const comms = require("./plugins/comms.js");
const javascriptBuild = require("./plugins/javascriptBuild.js");
const cssBuild = require("./plugins/cssBuild.js");

module.exports = function(eleventyConfig) {
  // plugins
  eleventyConfig.addPlugin(markdownHandling);
  eleventyConfig.addPlugin(handlingTime);
  eleventyConfig.addPlugin(helpers);
  eleventyConfig.addPlugin(comms);
  eleventyConfig.addPlugin(javascriptBuild);
  eleventyConfig.addPlugin(cssBuild);

  // add a custom collections for
  eleventyConfig.addCollection("subcalendars", function(collectionApi) {
    return collectionApi.getFilteredByGlob(
      "./src/contents/subcalendars/*-cal.md"
    );
  });

  eleventyConfig.setTemplateFormats(["njk", "md"]);

  // trying to put things through snowpack and not eleventy
  // eleventyConfig.addPassthroughCopy({ "static/js": "/js" });
  // eleventyConfig.addPassthroughCopy({ "static/css": "/css" });
  eleventyConfig.addPassthroughCopy({ "static/data": "/data" });
  eleventyConfig.addPassthroughCopy({ "static/images": "/images" });
  eleventyConfig.addPassthroughCopy({ "static/fonts": "/fonts" });

  // handle js and handle css
  // how?

  // bust
  eleventyConfig.addFilter("bust", (url) => {
    let local = new Date();
    return `${url}?v=${local.getTime()}`;
  });

  // folder system
  return {
    dir: {
      input: "src",
      output: "public",
      data: "data",
      includes: "layouts",
    },
    passthroughFileCopy: true,
  };
};
