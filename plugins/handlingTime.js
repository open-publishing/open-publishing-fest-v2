const momentz = require("moment-timezone");

module.exports = function(eleventyConfig) {
  eleventyConfig.addFilter("toUTC", function(date, timezone) {
    return momentz(`${date}`, timezone).utc().format();
  });

  // format date using moment
  eleventyConfig.addFilter(
    "formatDate",
    function(value, targetTimeZone = "UTC", format) {
      return momentz(value).tz(targetTimeZone).format(format);
    }
  );
  // find offset between dates using moment: this was used to show empty dates between events, but it needs to be rewritten.
  eleventyConfig.addFilter("momentInterval", function (lastDate, firstDate) {
    const start = momentz(firstDate);
    const end = momentz(lastDate);
    let offset = end.diff(start, "days");
    let emptyDays = ``;
    let dayPassed = 0;
    while (dayPassed < offset) {
      emptyDays += `<ul class="day empty ${momentz(firstDate)
        .add(dayPassed + 1, "days")
        .format("dddd")}" id="day-${momentz(firstDate)
          .add(dayPassed + 1, "days")
          .format("DD")}"><h2><span class="day-letter">${momentz(firstDate)
            .add(dayPassed + 1, "days")
            .format("dddd")}</span> <span class="number">${momentz(firstDate)
              .add(dayPassed + 1, "days")
              .format("DD")}</span></h2></ul>`;
      dayPassed = dayPassed + 1;
    }
    return emptyDays;
  });
  
};
