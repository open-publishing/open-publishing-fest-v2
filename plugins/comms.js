const fg = require("fast-glob");
// plugins to manipulate images for communications
module.exports = function (eleventyConfig) {
  // Run search for images in /images/comms/ and /sponsors
  const comImages = fg.sync(["**/images/comms/*", "!**/temp", "!**/public"]);

  //Create collection of gallery images
  eleventyConfig.addCollection("commsImages", function (collection) {
    return comImages;
  });
};
