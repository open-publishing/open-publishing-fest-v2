const postcss = require("postcss");
const postcssNested = require("postcss-nested");
const postcssImport = require("postcss-import");
const fs = require("fs");
const path = require("path");

const directoryPath = "public/css/themes/opf";

module.exports = function(eleventyConfig) {
  eleventyConfig.addWatchTarget("./static/css/");


  eleventyConfig.on("eleventy.before", async () => {
    console.log("generating-css");

    // Create the directory if it doesn't exist
    if (!fs.existsSync(directoryPath)) {
      fs.mkdirSync(directoryPath, { recursive: true });
    }

    fs.readFile("./static/css/themes/opf/main.css", async (err, css) => {
      await postcss([postcssImport, postcssNested])
        .process(css, {
          from: "./static/css/themes/opf/main.css",
          to: path.join(directoryPath, "main.css"),
        })
        .then((result) => {
          fs.writeFileSync(
            path.join(directoryPath, "main.css"),
            result.css,
            () => true
          );
          if (result.map) {
            fs.writeFileSync(
              path.join(directoryPath, "main.css.map"),
              result.map.toString(),
              () => true
            );
          }
        });
    });
  });
};
