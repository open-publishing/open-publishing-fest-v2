const markdownIt = require("markdown-it");
const implicitFigures = require("markdown-it-implicit-figures");
const blockEmbedPlugin = require("markdown-it-block-embed");
const html5Embed = require("markdown-it-html5-embed");
const frame = require("markdown-it-iframe");

module.exports = function(eleventyConfig) {


  eleventyConfig.addFilter("separateServices", function (value) {
    let values = value.replace(/(\r\n\t|\n|\r\t)/gm, " ,").split(",");
    // console.log(values);
    let cleanSocialAccount = "";
    // dont do anything if the value is empty
    values.forEach(socialAccount => {
      if (socialAccount != "") {
        const name = socialAccount.split(": ");
        let link;
        if (name[0].toLowerCase() === "twitter") {
          link = `<a target="_blank" class="twitter" href="https://www.twitter.com/${name[1].replace('@', '')}/">${name[1]}</a>`;
        }
        else if (name[0].toLowerCase() === "instagram" || name[0].toLowerCase() === "ig") {
          link = `<a target="_blank" class="instagram" href="https://www.instagram.com/${name[1].replace('@', '')}/">${name[1]}</a>`;
        } else if (name[0].toLowerCase() === "linkedin") {
          link = `<a target="_blank" class="linkedin" href="${name[1]}">${name[1].replace('https://www.linkedin.com/in/', '')}</a>`;
        }
        else {
          link = name[1];
        }
        cleanSocialAccount += (name.length > 1) ? `<li><span class="service">${name[0]}</span> <span class="handler">${link}</span></li>` : "";
      }
    })
    return cleanSocialAccount;
  });


  let markdownLib = markdownIt({ html: true })
    .use(implicitFigures, {
      dataType: false, // <figure data-type="image">, default: false
      figcaption: false, // <figcaption>alternative text</figcaption>, default: false
      tabindex: true, // <figure tabindex="1+n">..., default: false
      link: true, // <a href="img.png"><img src="img.png"></a>, default: false
    })
    .use(frame)
    .use(blockEmbedPlugin, {
      containerClassName: "video-embed",
      outputPlayerSize: false,
    })
    .use(html5Embed, {
      html5embed: {
        useImageSyntax: true, // Enables video/audio embed with ![]() syntax (default)
        useLinkSyntax: true, // Enables video/audio embed with []() syntax
      },
    });

  eleventyConfig.setLibrary("md", markdownLib);

  eleventyConfig.addFilter("markdownify", (value) => markdownLib.render(value));
  eleventyConfig.addFilter("markdownifyInline", (value) =>
    markdownLib.renderInline(value)
  );
};
