const esbuild = require("esbuild");

module.exports = function(eleventyConfig) {
  eleventyConfig.addWatchTarget("./static/js/");
  eleventyConfig.on("eleventy.after", async () => {
    console.log("generatingjs");
    await esbuild.build({
      entryPoints: [
        "static/js/attendees.js",
        "static/js/calendar.js",
        "static/js/form.js",
        "static/js/manage.js",
        "static/js/participant.js",
        "static/js/sponsors.js",
        "static/js/update.js",
      ], // Input file paths
      bundle: true,
      outdir: "public/js/", // Output directory
      // format: "esm",
      // splitting: true,
      // chunkNames: "chunks/[name]-[hash]",
      outbase: "static/js/", // Optional base directory for input paths
    });
  });
};
