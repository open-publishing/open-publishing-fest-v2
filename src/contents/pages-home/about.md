---
layout: singlepage.njk
title: "How will we shape the knowledge that will shape the future?"
class: "about"
tags: 
  - about
permalink: /about.html
---



<p>Open Publishing Fest celebrates communities developing open creative, scholarly, technological, and civic publishing projects. Together, we find new ground to share our ideas.</p>
<p>The fest is built on a desire to cheer people up and, at the same time, showcase a revolution in publishing occurring around the world and across sectors and industries. It’s an opportunity to bring people together in many ways, allowing participants to host sessions in their own style and on their own terms.</p>
<p>This is at once a collaborative and distributed event. Sessions are hosted by individuals and organizations around the world as panel discussions, fireside chats, demonstrations, workshops, and performances etc. </p>
<p>Join us by proposing a session. Proposals will be considered on a rolling basis up to and throughout the fest. You choose:

- your topic
- your participants
- your timezone and time
- your technology (for streaming etc)
- your language

Pretty much its about **you** and the event or events you want to host. Indulge! We will only filter out topics that are not about open publishing in someway.
</p>

If you dont know where to start with your setup, we made a small collection of links and options in the [Event toolkit](/toolkit.html) 

<!-- <p class="proposal-wrapper"><a class="proposal" href="form.html">Propose an event!</a></p> -->


<p>Feel free to propose an event for inclusion using the proposal form.</p>
<p>Please use the <a href="https://twitter.com/search?q=%23openpublish">#OpenPublish</a> tag when tweeting!</p>


<p>For more info or to discuss ideas please email <a href="mailto:adam@coko.foundation">adam@coko.foundation</a>
</p>

<h2>Want to help us pay for this awesome event?</h2>
<p>We are open to sponsorship although we are not at this time actively pursuing it (because we don't have the time). If you would like to help us find $ to pay for the event or would like to sponsor the event then drop Adam a line! <a href="mailto:adam@coko.foundation">adam@coko.foundation</a></p>

<h2>Participant Guidelines</h2>
<p>You can find the Participant Guidelines for the Open Publishing Fest <a href="/conduct.html">here</a>.</p>

<h2>Credits</h2>
<p>Concept: <a href="mailto:adam@coko.foundation">Adam Hyde</a> (Coko)</p>
<p>Organisation: Adam Hyde and Julien Taquet (Coko)</p>
<p>Curation: Adam Hyde, John Chodacki</p>
<p>Site Design and Dev: Julien Taquet (Coko)</p>
<p>Illustrations for 2021 from Agathe Baëz (using the amazing typeface Infini designed by Sandrine Nugue)</p>
<p>Video: Rob Blake (<a href="www.robblake.tv">www.robblake.tv</a>)</p>
<p>Sys Ops: Cloud 68 (<a href="https://cloud68.co/about">Cloud68.co/</a>)</p>
<p>$ : this event is financed by <a href="https://coko.foundation">Coko</a></p>
<p>The fonts used are <a href="https://fonts.google.com/specimen/Hepta+Slab#about"> Hepta Slab</a> from Michael LaGattuta and <a href="http://overpassfont.org/">Overpass</a> from <a href="https://delvefonts.com/">Delve Fonts</a>, sponsored by <a href="https://www.redhat.com/en">Red Hat</a></p>
<p>All code is open source. The calendar code developed for this event can be found at <a href="https://gitlab.coko.foundation/open-publishing/openpublishingfest">https://gitlab.coko.foundation/open-publishing/openpublishingfest</a></p>
<p>We are more than happy to talk to anyone that would like to organise a similar event on how we did it and what we learned.</p>
