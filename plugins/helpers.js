module.exports = function (eleventyConfig) {
  eleventyConfig.addFilter("replace", function (value, replace, replaceby) {
    return (value = value.replace(replace, replaceby));
  });

  eleventyConfig.addFilter("toLowerCase", function (value) {
    return (value = value.toLowerCase());
  });

  // filterBy key
  eleventyConfig.addFilter("filterBy", function (arr = [], key = "", value) {
    if (!Array.isArray(arr)) return arr;
    return arr.filter((item) => {
      if (item[key] == value) return item;
    });
  });

  eleventyConfig.addFilter("filtercal", function (arr = [], key = "", value) {
    if (!Array.isArray(arr)) return arr;
    return arr.filter((item) => {
      if (item.calendarid == value) return item;
    });
  });


  //sort by id
  eleventyConfig.addFilter("sortByID", function (value) {
    console.log("va", value);
    if (value == []) {
      return "";
    }
    return value.sort(function (a, b) {
      return b.id - a.id;
    });
  });

  // splitter between element
  eleventyConfig.addFilter("splitter", function (value, sep = ",") {
    if (value.includes(sep)) {
      return value.split(sep);
    }
  });

  //filter to check if an entry contains a data
  eleventyConfig.addFilter("where", function (array, key, value) {
    return array.filter((item) => {
      const keys = key.split(".");
      const reducedKey = keys.reduce((object, key) => {
        return object[key];
      }, item);

      return reducedKey === value ? item : false;
    });
  });
};
